#!/bin/sh

module=$1
echo "Build go micronode"
cd /go-modules/go-micronode
GO111MODULE=on go mod vendor
cp -Rp /go-modules/librdkafka ./vendor/gopkg.in/confluentinc/confluent-kafka-go.v1/kafka
GO111MODULE=on go build -mod=vendor
#zip -r $module.zip micronode config.json
if [ -d "/go-modules/go-micronode/conf" ]
then
zip -r $module.zip micronode service.yml conf/
else
zip -r $module.zip micronode service.yml
fi

ls -la
