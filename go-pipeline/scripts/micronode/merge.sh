#!/bin/sh
source_path=$1
dest_path=$2
echo "Merge script is execute."
cd $source_path
find . -type f -not -iname 'go.*' -not -path './.git/*' -not -path './.gitignore' -not -path 'service_plugin.go' -not -path 'service_accessor.go' -not -path 'service_controller.go' -print0 -prune -o -depth | cpio -0pdv --quiet $dest_path
ls -la $dest_path/service