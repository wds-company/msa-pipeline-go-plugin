def execute(config) {
    try {
        parseJson = readJSON text: config
    } catch (Exception e) {
        error("Cannot read json config.\nError:\n${e}")
    }
    try {
        if (parseJson['container']) {
            env.containerName = parseJson['container']['name']
            env.imageName = parseJson['container']['image']
            env.dockerInputFile = parseJson['container']['docker_file']
            env.dockerComposeFile = parseJson['container']['compose_file']
            env.imageVersion = parseJson['container']['image_version']
        }
        if (parseJson['template']) {
            env.templateName = parseJson['template']['name']
            env.templatePackage = parseJson['template']['package']
            env.templateSource = parseJson['template']['source']
            env.templateVersion = parseJson['template']['version']
        }
        if (parseJson['plugin']) {
            env.pluginId = parseJson['plugin']['id']
            env.pluginModule = parseJson['plugin']['module']
            env.pluginModuleGroup = parseJson['plugin']['module_group']
            env.pluginPackage = parseJson['plugin']['package']
            env.pluginSource = parseJson['plugin']['source']
            env.pluginVersion = parseJson['plugin']['version']
        }

        return parseJson
    } catch (Exception e) {
        println "Cannot load build configuration.\nError:\n${e}"
        error("Cannot load build configuration.\nError:\n${e}")
    }
}

return this