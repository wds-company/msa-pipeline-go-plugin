def execute(configFile) {
    def exists = fileExists "${configFile}"
    println "test"
    if (exists) {
        echo "${configFile} found"
    } else {
        error("${configFile} cannot be found")
    }
    try {
        deployConfig = readJSON file: "${configFile}"
    } catch (Exception e) {
        error("Cannot read ${configFile} file.\nError:\n${e}")
    }
    try {
        env.templatePackage = deployConfig['template_package']
        env.pluginserverPackage = deployConfig['pluginserver_package']
        env.moduleDirectory = deployConfig["module_directory"]     
        env.libModulePath = deployConfig['lib_module_path']
        env.extLibModule = deployConfig['ext_lib_module']
        env.lineToken = deployConfig['line_token']
        env.moduleRootPath = env.WORKSPACE + '/' + env.moduleDirectory
        env.registryHost = deployConfig['registry_host']
        env.registryPort = deployConfig['registry_port']
        env.privateKey = deployConfig['private_key']
        env.registryUsername = deployConfig['registry_username']
        env.registryPassword = deployConfig['registry_password']

        if (deployConfig['container']) {
            env.containerName = deployConfig['container']['name']
            env.imageName = deployConfig['container']['image']
            env.dockerInputFile = deployConfig['container']['docker_file']
            env.dockerComposeFile = deployConfig['container']['compose_file']
            env.imageVersion = deployConfig['container']['image_version']
        }
        return deployConfig
    } catch (Exception e) {
        error("Cannot read ${configFile} property: deployment.files\nError:\n${e}")
    }
}

return this